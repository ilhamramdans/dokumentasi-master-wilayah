:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

API::
Singkatan dari _Application Programming Interface_, yaitu kumpulan definisi dan protokol terkait
koneksi antara komputer atau aplikasi yang memungkinkan untuk melakukan pertukaran data.

JWT::
Singkatan dari _JSON Web Token_, yaitu suatu standar untuk pertukaran data yang diperlukan dalam
proses autentikasi.

MSI::
Data Fasyankes atau Master Sarana Index (MSI), merupakan standar data 35 fasyankes di Indonesia.
Disusun dari berbagai sumber, seperti Sistem Informasi Sumber Daya Manusia Kesehatan (SI-SDMK), Sistem Informasi Manajemen Data Kefarmasian (SIMADA), RS Online, dan lain sebagainya.
