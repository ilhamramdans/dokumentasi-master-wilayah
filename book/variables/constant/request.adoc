// mimes
:mime-urlencoded: application/x-www-form-urlencoded
:mime-formdata: multipart/form-data
:mime-json: application/json
:mime-text: text/plain

// http headers
:http-header-authz: Authorization
:http-header-content: Content-Type
:http-header-accept: Accept

// http client
:curl: cURL
:postman: Postman
:postman-at-input-key: pass:q,a[pada kotak masukan pada kolom *KEY*]
:postman-at-input-value: pass:q,a[pada kotak masukan pada kolom *VALUE*]
:postman-body-none: none
:postman-body-formdata: form-data
:postman-body-urlencoded: x-www-form-urlencoded
:postman-body-raw: raw
:postman-body-raw-text: Text
:postman-body-raw-json: JSON
:postman-body-raw-html: HTML
:postman-body-raw-xml: XML
:postman-body-binary: binary
:postman-body-graphql: GraphQL
