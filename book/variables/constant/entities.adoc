// Entity or organization related variables, also you can use pre-defined variables from:
// - book/attributes
// - book/variables/constants
:entity-dto: Digital Transformation Office
:entity-dto-alt: DTO
:entity-kemkes: Kementerian Kesehatan Republik Indonesia
:entity-kemkes-alt: Kemkes RI
:entity-pusdatin: Pusat Data dan Teknologi Informasi
:entity-pusdatin-alt: Pusdatin
:entity-google: Google
:entity-amazon: Amazon
