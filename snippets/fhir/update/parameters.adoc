// Request parameter orders are: Path, Header, Query String, Body

===== Parameter Path URI

[cols="2,^1,5a",separator="|"]
|===
|Nama Parameter |Tipe Data |Keterangan

|`*:id`
|`uuid`
|ID referensi dari _resource {fhir-resource}_ yang akan dilakukan proses pembaruan data (_update_).

|===

===== Header

[cols="2,^1,5a",separator="|"]
|===
|Nama Parameter |Tipe Data |Keterangan

include::{dir-snippet}/apidoc/req-param-auth-bearer.adoc[]

include::{dir-snippet}/apidoc/req-param-content-json.adoc[]

|===

===== Body (`{mime-json}`)

Di bagian _body_ ini _payload_ JSON dari _resource {fhir-resource}_ sesuai standar FHIR dimasukkan. Terkait cara pengisian dari format FHIR tersebut di luar cakupan dari dokumentasi ini, silakan melihat pada dokumentasi terkait *Petunjuk Teknis* atau *Playbook* yang telah disediakan oleh tim {satset} dari {subject-provider}.

Bentuk umum dari _payload_ untuk penambahan data sebagai berikut:

:res-file-name: req.jsonc
[source,json,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----
