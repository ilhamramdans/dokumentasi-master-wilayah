Hasil _response_, dengan HTTP _Status Code_ berpola `2xx` atau `4xx`, yang dikembalikan dari server mempunyai parameter `{http-header-content}` dengan nilai `{mime-json}` di salah satu parameter _header_-nya.

===== 2xx _Success_

Bila proses pembaruan data berhasil maka akan mengembalikan _payload_ dari _resource {fhir-resource}_ yang sebelumnya telah dikirim.

*Contoh Data*

:res-file-name: res-2xx.jsonc
[source,json,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----

===== 4xx _Client Error_

Sistem akan mengembalikan pesan _error_ bila _client_ belum melakukan autentikasi, tidak memiliki akses, menggunakan HTTP _method_ yang tidak tepat, atau mengirimkan data dengan format, parameter, atau ketentuan lainnya yang tidak sesuai atau tidak dimengerti oleh sistem.

*Contoh Data*

:res-file-name: res-4xx.jsonc
[source,json,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----

===== 5xx _Server Error_ (`{http-header-content}: {mime-text}`)
Sistem akan mengembalikan pesan _error_ bila terjadi kesalahan pada sisi server saat memproses data yang telah dikirimkan.

*Contoh Data*

:res-file-name: res-5xx.txt
[source,text,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----
